package org.dat92tashk.osservicesmanagement.environment.unix.systemd;

import org.dat92tashk.osservicesmanagement.Host;
import org.dat92tashk.osservicesmanagement.environment.Killable;
import org.dat92tashk.osservicesmanagement.executecmd.CommandExecute;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResults;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResultsCache;

public class SystemDKill extends SystemDShell implements Killable {

    public SystemDKill(CommandExecute executor, CommandResultsCache cache) {
        super(executor, cache);
    }

    @Override
    public boolean performKillService(Host.Service service) {
        return false;
    }

    @Override
    public String performGetPIDService(Host.Service service) {
        CommandResults results = getServiceParameters(service.name);
        final String MainPID = "Main PID: ";
        for (String curLine : results.getLines()) {
            if (curLine.contains(MainPID)) {
                curLine = curLine.substring(curLine.indexOf(":") + 1, curLine.indexOf("("));
                return curLine.replaceAll("\\D+", "");
            }
        }
        return "-1";
    }
}
