package org.dat92tashk.osservicesmanagement.environment;

import org.dat92tashk.osservicesmanagement.Host;

public interface Autostart extends ServiceOperation {
    void performMakeAutoRun(Host.Service service);
    void performUnmakeAutoRun(Host.Service service);
    boolean getAutoStartStatus(Host.Service service);
}
