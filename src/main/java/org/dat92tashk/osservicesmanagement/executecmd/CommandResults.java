package org.dat92tashk.osservicesmanagement.executecmd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandResults {
    private final List<String> lines;
    private final int exitCode;
    private final String cmd;

    public CommandResults(int exitCode, String cmd, String[] linesArray) {
        this.exitCode = exitCode;
        this.cmd = cmd;
        lines = new ArrayList<>(Arrays.asList(linesArray));
    }

    public List<String> getLines() {
        return lines;
    }

    public int getExitCode() {
        return exitCode;
    }

    public String getCmd() {
        return cmd;
    }
}
