package org.dat92tashk.osservicesmanagement;

public enum OSType {
    UNIX,
    Windows,
    Other
}
