package org.dat92tashk.osservicesmanagement;

import org.dat92tashk.osservicesmanagement.environment.unix.systemd.SystemDManage;
import org.dat92tashk.osservicesmanagement.executecmd.CommandExecute;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResults;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

public class SystemdMock {

    public static CommandExecute getSystemdMock() {
        CommandExecute mock = Mockito.mock(CommandExecute.class);

        CommandResults allDaemons = new CommandResults(0,
                SystemDManage.GET_DAEMON_STAT_CMD_TEMPLATE,
                getAllDaemons());

        Mockito
                .when(mock.execute(any()))
                .thenAnswer(new Answer<String[]>() {
                    @Override
                    public String[] answer(InvocationOnMock invocationOnMock) throws Throwable {
                        String cmd = (String) invocationOnMock.getArguments()[0];
                        switch (cmd) {
                            case SystemDManage.GET_ALL_DAEMONS_CMD:
                                return getAllDaemons();
                            default:
                                return null;
                        }
                    }
                });
        return mock;
    }

    private static String[] getAllDaemons() {
        return getMockData("systemdGetAllDaemons");
    }

    private static String[] getActiveStatus() {
        return getMockData("activeStatus");
    }

    private static String[] getFailedStatus() {
        return getMockData("failedStatus");
    }

    private static String[] getInactiveStatus() {
        return getMockData("inactiveStatus");
    }

    private static String[] getMockData(String fileName) {
        List<String> data;
        String filePath = String.format("target%stest-classes%s%s",
                File.separatorChar, File.separatorChar, fileName);
        try {
            data = Files.readAllLines(
                    Paths.get(filePath),
                    StandardCharsets.UTF_8);
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return data.toArray(new String[0]);
    }
}
