package org.dat92tashk.osservicesmanagement.environment;

import org.dat92tashk.osservicesmanagement.Host;
import org.dat92tashk.osservicesmanagement.Manageable;

import java.util.Collections;
import java.util.List;

public class EmptySoftware implements
        Manageable,
        Autostart,
        DelayedStart,
        Descriptive,
        Disable,
        Killable,
        Reloaded {
    @Override
    public boolean performStartService(Host.Service service) {
        return false;
    }

    @Override
    public boolean performStopService(Host.Service service) {
        return false;
    }

    @Override
    public List<String> performGetNamesAllExistServices() {
        return Collections.emptyList();
    }

    @Override
    public Host.ServiceStatus performGetServiceStatus(Host.Service service) {
        return Host.ServiceStatus.Unknown;
    }

    @Override
    public void performMakeAutoRun(Host.Service service) {

    }

    @Override
    public void performUnmakeAutoRun(Host.Service service) {

    }

    @Override
    public boolean getAutoStartStatus(Host.Service service) {
        return false;
    }

    @Override
    public void performMakeDelayedAutoRun(Host.Service service) {

    }

    @Override
    public void performUnmakeDelayedAutoRun(Host.Service service) {

    }

    @Override
    public boolean getDelayedStartStatus(Host.Service service) {
        return false;
    }

    @Override
    public String performGetServiceDescription(Host.Service service) {
        return "";
    }

    @Override
    public void performDisableService(Host.Service service) {

    }

    @Override
    public void performEnableService(Host.Service service) {

    }

    @Override
    public boolean performGetDisabledStatus(Host.Service service) {
        return false;
    }

    @Override
    public boolean performKillService(Host.Service service) {
        return false;
    }

    @Override
    public String performGetPIDService(Host.Service service) {
        return "-1";
    }

    @Override
    public void performReloadService(Host.Service service) {

    }
}
