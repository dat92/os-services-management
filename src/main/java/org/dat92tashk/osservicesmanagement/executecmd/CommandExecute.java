package org.dat92tashk.osservicesmanagement.executecmd;

public interface CommandExecute {
    CommandResults execute(String cmd);
}
