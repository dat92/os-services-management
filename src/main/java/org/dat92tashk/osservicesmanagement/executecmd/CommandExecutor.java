package org.dat92tashk.osservicesmanagement.executecmd;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.PumpStreamHandler;
import org.dat92tashk.osservicesmanagement.Host;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MarkerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Arrays;

public class CommandExecutor implements CommandExecute {
    private static final Logger logger
            = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());


    @Override
    public CommandResults execute(String cmd) {
        CommandLine cmdLine = CommandLine.parse(cmd);
        DefaultExecutor executor = new DefaultExecutor();
        int exitCode = -1;

        ByteArrayOutputStream oas = new ByteArrayOutputStream();
        PumpStreamHandler streamHandler = new PumpStreamHandler(oas);
        executor.setStreamHandler(streamHandler);
        try {
            logger.debug("Executed command \"{}\"", cmd);
            exitCode = executor.execute(cmdLine);
        }
        catch (ExecuteException ex) {
            exitCode = ex.getExitValue();
            logger.debug("An exception occurred during execution " +
                    "the command \"{}\" with exit code={}", cmd, exitCode);
            logger.debug("Exception:", ex);
        }
        catch (IOException e) {
            logger.error("Error during execution command \"{}\"", cmd, e);
        }

        String[] resultArray = new String(oas.toByteArray()).split(new String("\n"));
        if (exitCode == 0) {
            logger.debug("Command \"{}\" was successfully executed with exit code {}", cmd, exitCode);
        }
        if (logger.isTraceEnabled()) {
            //TODO Подумать: стоит ли это в лог включать
            StringBuffer sb = new StringBuffer();
            Arrays.stream(resultArray).forEach(x -> sb.append(x).append("\n"));
            logger.trace(MarkerFactory.getMarker(Host.EXEC_OUT),
                    "Result of \"{}\" command:\n{}", cmd, sb);
        }
        return new CommandResults(exitCode, cmd, resultArray);
    }
}
