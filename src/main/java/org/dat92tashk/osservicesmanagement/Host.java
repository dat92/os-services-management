package org.dat92tashk.osservicesmanagement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Objects;

public abstract class Host {
    //Marker of command execute results
    public static final String EXEC_OUT = "EXEC_OUT";
    private static final Logger logger
            = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    public final String hostName;
    protected Manageable manageable;
    protected HashMap<String, Service> services = new HashMap<>();

    public Host(String hostName, Manageable manageable) {
        this.hostName = hostName;
        this.manageable = manageable;
        logger.debug("Host \"{}\" created with \"{}\" manageable value",
                hostName, manageable.getClass().getSimpleName());
    }

    public abstract OSType getHostType();
    public abstract void updateServiceParameters(String name);
    public abstract void rereadAllExistServices();

    public String getHostName() {
        return hostName;
    }

    public Manageable getManageable() {
        return manageable;
    }

    public void setManageable(Manageable manageable) {
        this.manageable = manageable;
        logger.debug("\"{}\" hosts manageable value changed to \"{}\"",
                hostName, manageable.getClass().getSimpleName());
    }

    public String[] getAllServicesNames() {
        return services.keySet().toArray(new String[0]);
    }

    public Service getService(String name) {
        return services.get(name);
    }

    public boolean serviceExist(String name) {
        return services.containsKey(name);
    }

    public boolean startService(Service service) {
        if (service.getStatus() == ServiceStatus.Active) return true;
        //TODO Добавить проверку что если статус != inactive, то иссключение
        //TODO Или добавить метод который будет форсировано пытаться запустить
        if (manageable.performStartService(service)) {
            service.previousStatus = service.status;
            service.status = ServiceStatus.Inactive;
            return true;
        }
        return false;
    }

    public boolean stopService(Service service) {
        if (service.getStatus() == ServiceStatus.Inactive) return true;
        if (manageable.performStopService(service)) {
            service.previousStatus = service.status;
            service.status = ServiceStatus.Inactive;
            return true;
        }
        return false;
    }

    public boolean restartService(Service service) {
        return stopService(service) & startService(service);
    }

    public enum ServiceStatus {
        Active, Inactive, ProcessPerfoming, Unknown
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Host host = (Host) o;
        return hostName.equals(host.hostName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hostName);
    }

    @Override
    public String toString() {
        return "Host = \"" + hostName + "\"";
    }

    public class Service {
        public final String name;
        protected ServiceStatus status;
        protected ServiceStatus previousStatus;

        protected Service(String name, ServiceStatus status) {
            this.name = name;
            this.status = status;
            previousStatus = ServiceStatus.Unknown;
        }

        protected Service(String name) {
            this.name = name;
            status = ServiceStatus.Unknown;
            previousStatus = ServiceStatus.Unknown;
        }

        public ServiceStatus getStatus() {
            return status;
        }

        public ServiceStatus getPreviousStatus() {
            return previousStatus;
        }

        public boolean start() {
            return startService(this);
        }

        public boolean stop() {
            return stopService(this);
        }

        public boolean restart() {
            return restartService(this);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Service service = (Service) o;
            return name.equals(service.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name);
        }

        @Override
        public String toString() {
            return "Service{" +
                    "name='" + name + '\'' +
                    ", status=" + status +
                    ", previousStatus=" + previousStatus +
                    '}';
        }
    }
}
