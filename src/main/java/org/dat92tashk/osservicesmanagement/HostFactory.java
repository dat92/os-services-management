package org.dat92tashk.osservicesmanagement;


import org.dat92tashk.osservicesmanagement.environment.*;
import org.dat92tashk.osservicesmanagement.environment.unix.UnixHost;
import org.dat92tashk.osservicesmanagement.environment.unix.systemd.*;
import org.dat92tashk.osservicesmanagement.environment.windows.WindowsHost;
import org.dat92tashk.osservicesmanagement.executecmd.CommandExecute;
import org.dat92tashk.osservicesmanagement.executecmd.CommandExecutor;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResultsCacheFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;

public class HostFactory {

    private static final Logger logger
            = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    private HostFactory() {}

    static OSType detectHostType() {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("win")) {
            return OSType.Windows;
        } else if (os.contains("nix") || os.contains("nux") || os.contains("mac")) {
            return OSType.UNIX;
        }
        else return OSType.Other;
    }

    private static CommandExecute getCommandExecute(ServicesManagementSoftware soft) {
        CommandExecute cmdExecutor;
        switch (soft.getSoftType()) {
            case Shell:
                cmdExecutor = new CommandExecutor();
                break;
            case Empty:
                cmdExecutor = null;
                break;
            default:
                String message = "Not supported software \"" + soft + "\"";
                logger.error(message);
                throw new IllegalArgumentException(message);
        }
        return cmdExecutor;
    }

    public static Host getHost(String name, ServicesManagementSoftware soft) {
        return getHost(name, detectHostType(), soft, getCommandExecute(soft));
    }

    public static Host getHost(String name, OSType os, ServicesManagementSoftware soft) {
        return getHost(name, os, soft, getCommandExecute(soft));
    }

    static Host getHost(String name, OSType os,
                               ServicesManagementSoftware soft, CommandExecute cmdExecutor) {
        switch (os) {
            case UNIX:
                return getUnixHost(name, soft, cmdExecutor);
            case Windows:
                return getWindowsHost(name, soft);
            case Other:
                return getAbstractHost(name, soft);
            default:
                String message = "Unsupported OS \"" + os + "\"";
                logger.error(message);
                throw new IllegalArgumentException(message);
        }
    }

    private static UnixHost getUnixHost(String hostName,
                                        ServicesManagementSoftware soft,
                                        CommandExecute cmdExecutor) {
        logger.info("Performing provision Unix host \"{}\"" +
                " with services management software \"{}\"", hostName, soft);
        boolean notSupport = true;

        for (OSType cur : soft.getSupportedOS()) {
            if (cur == OSType.UNIX) {
                notSupport = false;
                break;
            }
        }

        if (notSupport) {
            String message = String.format(
                    "Services management software %s not supported UNIX host", soft);
            logger.error(message);
            throw new IllegalArgumentException(message);
        }


        Manageable manage;
        Killable kill;
        Disable disable;
        Reloaded reloaded;
        Autostart autostart;
        Descriptive descriptive;


        switch (soft) {
            case SystemDShell:
                manage = new SystemDManage(cmdExecutor,
                        CommandResultsCacheFactory.getCache(hostName));
                kill = new SystemDKill(cmdExecutor,
                        CommandResultsCacheFactory.getCache(hostName));
                disable = new SystemDDisable(cmdExecutor,
                        CommandResultsCacheFactory.getCache(hostName));
                reloaded = new SystemDReload(cmdExecutor,
                        CommandResultsCacheFactory.getCache(hostName));
                autostart = new SystemDAutoStart(cmdExecutor,
                        CommandResultsCacheFactory.getCache(hostName));
                descriptive = new SystemDDescription(cmdExecutor,
                        CommandResultsCacheFactory.getCache(hostName));
                logger.info("Set \"{}\" services management software", ServicesManagementSoftware.SystemDShell);
                break;
            case Empty:
                ServiceOperation serviceOperation = new EmptySoftware();
                manage = (Manageable) serviceOperation;
                kill = (Killable) serviceOperation;
                disable = (Disable) serviceOperation;
                reloaded = (Reloaded) serviceOperation;
                autostart = (Autostart) serviceOperation;
                descriptive = (Descriptive) serviceOperation;
                logger.info("Set \"{}\" services management software", ServicesManagementSoftware.Empty);
                break;
            default:
                String message = String.format("Unknown services management type=%s", soft);
                logger.error(message);
                throw new IllegalArgumentException(message);
        }
        return new UnixHost(hostName, manage, autostart, kill, disable, reloaded, descriptive);

    }

    private static WindowsHost getWindowsHost(String name, ServicesManagementSoftware servicesManagementSoftware) {
        return null;
    }

    private static Host getAbstractHost(String name, ServicesManagementSoftware servicesManagementSoftware) {
        return null;
    }
}

