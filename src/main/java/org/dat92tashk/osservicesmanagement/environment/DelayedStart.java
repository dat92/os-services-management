package org.dat92tashk.osservicesmanagement.environment;

import org.dat92tashk.osservicesmanagement.Host;

public interface DelayedStart extends ServiceOperation {
    void performMakeDelayedAutoRun(Host.Service service);
    void performUnmakeDelayedAutoRun(Host.Service service);
    boolean getDelayedStartStatus(Host.Service service);
}
