package org.dat92tashk.osservicesmanagement.environment;

import org.dat92tashk.osservicesmanagement.Host;

public interface Descriptive extends ServiceOperation {
    String performGetServiceDescription(Host.Service service);
}
