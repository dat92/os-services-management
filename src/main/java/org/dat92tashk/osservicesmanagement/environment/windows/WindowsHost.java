package org.dat92tashk.osservicesmanagement.environment.windows;

import org.dat92tashk.osservicesmanagement.Host;
import org.dat92tashk.osservicesmanagement.Manageable;
import org.dat92tashk.osservicesmanagement.OSType;
import org.dat92tashk.osservicesmanagement.environment.Autostart;
import org.dat92tashk.osservicesmanagement.environment.DelayedStart;
import org.dat92tashk.osservicesmanagement.environment.Disable;
import org.dat92tashk.osservicesmanagement.environment.Killable;

public class WindowsHost extends Host {
    private Autostart autostart;
    private Disable disable;
    private Killable kill;
    private DelayedStart delayed;

    public WindowsHost(String hostName,
                       Manageable manageable,
                       Autostart autostart,
                       Disable disable,
                       Killable kill,
                       DelayedStart delayed) {
        super(hostName, manageable);
        this.autostart = autostart;
        this.disable = disable;
        this.kill = kill;
        this.delayed = delayed;
    }

    public Autostart getAutostart() {
        return autostart;
    }

    public void setAutostart(Autostart autostart) {
        this.autostart = autostart;
    }

    public Disable getDisable() {
        return disable;
    }

    public void setDisable(Disable disable) {
        this.disable = disable;
    }

    public Killable getKill() {
        return kill;
    }

    public void setKill(Killable kill) {
        this.kill = kill;
    }

    public DelayedStart getDelayed() {
        return delayed;
    }

    public void setDelayed(DelayedStart delayed) {
        this.delayed = delayed;
    }

    @Override
    public OSType getHostType() {
        return OSType.Windows;
    }

    @Override
    public void updateServiceParameters(String name) {

    }

    @Override
    public void rereadAllExistServices() {

    }

    public void killService(WindowsService service) {
        kill.performKillService(service);
    }

    public void enableService(WindowsService service) {
        disable.performEnableService(service);
    }

    public void disableService(WindowsService service) {
        disable.performDisableService(service);
    }

    public void enableAutoStart(WindowsService service) {
        autostart.performMakeAutoRun(service);
    }

    public void disableAutoStart(WindowsService service) {
        autostart.performUnmakeAutoRun(service);
    }

    public void enableAutoStartDelayed(WindowsService service) {
        delayed.performMakeDelayedAutoRun(service);
    }

    public void disableAutoStartDelayed(WindowsService service) {
        delayed.performUnmakeDelayedAutoRun(service);
    }

    public class WindowsService extends Service {
        private int pid;
        private boolean disabled;
        private boolean autorun;
        private boolean autorunDelayed;
        public final String displayName;

        protected WindowsService(String name,
                                 String displayName,
                                 ServiceStatus status) {
            super(name, status);
            this.displayName = displayName;
        }

        public int getPid() {
            return pid;
        }

        public boolean isDisabled() {
            return disabled;
        }

        public boolean isAutorun() {
            return autorun;
        }

        public boolean isAutorunDelayed() {
            return autorunDelayed;
        }

        @Override
        public String toString() {
            return "WindowsService{" +
                    "pid=" + pid +
                    ", disabled=" + disabled +
                    ", autorun=" + autorun +
                    ", autorunDelayed=" + autorunDelayed +
                    ", displayName='" + displayName + '\'' +
                    ", name='" + name + '\'' +
                    ", status=" + status +
                    ", previousStatus=" + previousStatus +
                    '}';
        }
    }
}
