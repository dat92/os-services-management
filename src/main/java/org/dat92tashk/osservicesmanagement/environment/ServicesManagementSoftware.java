package org.dat92tashk.osservicesmanagement.environment;

import org.dat92tashk.osservicesmanagement.OSType;

public enum ServicesManagementSoftware {
    SystemDShell(SoftType.Shell, OSType.UNIX),
    SCCmd(SoftType.Shell, OSType.Windows),
    NetCmd(SoftType.Shell, OSType.Windows),
    Empty(SoftType.Empty, OSType.UNIX, OSType.Windows, OSType.Other);

    private OSType[] supportedOS;
    private SoftType softType;

    ServicesManagementSoftware(SoftType softType, OSType ... os) {
        supportedOS = os;
        this.softType = softType;
    }

    public enum SoftType {
        Shell,
        Empty
    }

    public SoftType getSoftType() {
        return softType;
    }

    public OSType[] getSupportedOS() {
        return supportedOS;
    }
}
