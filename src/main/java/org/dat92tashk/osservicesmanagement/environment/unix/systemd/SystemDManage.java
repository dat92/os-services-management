package org.dat92tashk.osservicesmanagement.environment.unix.systemd;

import org.dat92tashk.osservicesmanagement.Host;
import org.dat92tashk.osservicesmanagement.Manageable;
import org.dat92tashk.osservicesmanagement.executecmd.CommandExecute;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResults;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResultsCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class SystemDManage extends SystemDShell implements Manageable {
    private static final Logger logger
            = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    public static final String GET_ALL_DAEMONS_CMD = "systemctl list-units -t service --all --no-legend";
    public static final String GET_DAEMON_STAT_CMD_TEMPLATE = "systemctl is-active \"%s\"";


    public SystemDManage(CommandExecute executor, CommandResultsCache cache) {
        super(executor, cache);
    }


    @Override
    public boolean performStartService(Host.Service service) {
        return false;
    }

    @Override
    public boolean performStopService(Host.Service service) {
        return false;
    }

    @Override
    public List<String> performGetNamesAllExistServices() {
        logger.debug("Performing getting names of all exist daemons");
        CommandResults result = executeCommand(SystemDManage.GET_ALL_DAEMONS_CMD);
        if (result.getExitCode() != 0) {
            logger.error("Retrieving list of daemons names failed with exit code {}",
                    result.getExitCode());
            return Collections.emptyList();
        }
        List<String> servicesName = new LinkedList<>();
        for (String curLine : result.getLines()) {
            servicesName.add(curLine.substring(0, curLine.indexOf(" ")));
        }
        logger.debug("Retrieving list of daemons names was success");
        return servicesName;
    }

    @Override
    public Host.ServiceStatus performGetServiceStatus(Host.Service service) {
        logger.debug("Retrieving daemon \"{}\" status", service.name);
        CommandResults result = executeCommand(
                String.format(SystemDManage.GET_DAEMON_STAT_CMD_TEMPLATE, service.name));

        //Exit code 0 of systemctl is mean daemon active

        if (result.getExitCode() == 0) {
            logger.debug("For daemon \"{}\" exit code command = {}. Daemon is active.",
                    service.name, result.getExitCode());
            return Host.ServiceStatus.Active;
        } else {
            logger.debug("For daemon \"{}\" exit code command = {}. Daemon is inactive.",
                    service.name, result.getExitCode());
            return Host.ServiceStatus.Inactive;
        }
    }
}
