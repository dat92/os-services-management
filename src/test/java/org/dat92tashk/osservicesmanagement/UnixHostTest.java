package org.dat92tashk.osservicesmanagement;

import org.dat92tashk.osservicesmanagement.environment.ServicesManagementSoftware;
import org.dat92tashk.osservicesmanagement.environment.unix.UnixHost;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class UnixHostTest {
    private static UnixHost host;

    @BeforeAll
    private static void initHost() {
        Host emptyHost =
                HostFactory.getHost(
                        "localhost",
                        OSType.UNIX,
                        ServicesManagementSoftware.Empty);
        host = (UnixHost) emptyHost;
        host.setManageable(new ServicesManageMock());
        host.rereadAllExistServices();
    }

    @Test
    void testIdenticalDaemons() {
        /*
        After reread, existed daemons should not be recreating.
        Daemons is creating with default status "Unknown"
         */
        host.rereadAllExistServices();
        assertSame(host.getDaemon("Service-0").previousStatus, Host.ServiceStatus.Active);
        assertSame(host.getDaemon("Service-3").status, Host.ServiceStatus.Inactive);
        assertSame(host.getDaemon("Service-7").status, Host.ServiceStatus.ProcessPerfoming);
    }
}