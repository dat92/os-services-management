package org.dat92tashk.osservicesmanagement;

import java.util.LinkedList;
import java.util.List;

public class ServicesManageMock implements Manageable {


    @Override
    public boolean performStartService(Host.Service service) {
        switch (service.name) {
            case "Service-0":
            case "Service-1":
            case "Service-2":
            case "Service-3":
            case "Service-4":
            case "Service-9":
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean performStopService(Host.Service service) {
        switch (service.name) {
            case "Service-0":
            case "Service-1":
            case "Service-3":
            case "Service-4":
            case "Service-9":
                return true;
            default:
                return false;
        }
    }

    @Override
    public List<String> performGetNamesAllExistServices() {
        LinkedList<String> services = new LinkedList<>();
        for (int i = 0; i < 12; i++) {
            services.add(String.format("Service-%d", i));
        }
        return services;
    }

    @Override
    public Host.ServiceStatus performGetServiceStatus(Host.Service service) {
        switch (service.name) {
            case "Service-0":
            case "Service-1":
            case "Service-2":
                return Host.ServiceStatus.Active;
            case "Service-3":
            case "Service-4":
            case "Service-5":
                return Host.ServiceStatus.Inactive;
            case "Service-6":
            case "Service-7":
            case "Service-8":
                return Host.ServiceStatus.ProcessPerfoming;
            default:
                return Host.ServiceStatus.Unknown;
        }
    }
}
