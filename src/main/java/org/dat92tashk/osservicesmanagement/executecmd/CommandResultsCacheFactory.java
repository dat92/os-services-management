package org.dat92tashk.osservicesmanagement.executecmd;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.time.Duration;

public class CommandResultsCacheFactory {
    private static final Logger logger
            = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    private CommandResultsCacheFactory() {}

    private static volatile Cache<String, CommandResultsCache> hosts = CacheBuilder.newBuilder()
            .expireAfterWrite(Duration.ofHours(1))
            .build(new CacheLoader<String, CommandResultsCache>() {
                @Override
                public CommandResultsCache load(String s) throws Exception {
                    return null;
                }
            });

    public static synchronized CommandResultsCache getCache(String hostName) {
        logger.trace("Getting CommandResultsCache for \"{}\" host", hostName);
        CommandResultsCache cache = hosts.getIfPresent(hostName);
        if (cache == null) {
            logger.trace("CommandResultsCache for \"{}\" host missing in cache" +
                            " and will be created",
                    hostName);
            cache = new CommandResultsCache();
            hosts.put(hostName, cache);
        } else {
            logger.trace("CommandResultsCache for \"{}\" host" +
                    " was obtained from cache", hostName);
        }
        return cache;
    }
}
