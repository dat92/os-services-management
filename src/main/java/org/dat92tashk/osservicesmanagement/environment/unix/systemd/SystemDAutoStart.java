package org.dat92tashk.osservicesmanagement.environment.unix.systemd;

import org.dat92tashk.osservicesmanagement.Host;
import org.dat92tashk.osservicesmanagement.environment.Autostart;
import org.dat92tashk.osservicesmanagement.executecmd.CommandExecute;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResults;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResultsCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;

public class SystemDAutoStart extends SystemDShell implements Autostart {
    private static final Logger logger
            = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    public static final String GET_AUTO_START_STATUS_CMD_TEMPLATE = "systemctl is-enabled \"%s\"";

    public SystemDAutoStart(CommandExecute executor, CommandResultsCache cache) {
        super(executor, cache);
    }

    public void performMakeAutoRun(Host.Service service) {

    }

    @Override
    public void performUnmakeAutoRun(Host.Service service) {

    }

    @Override
    public boolean getAutoStartStatus(Host.Service service) {
        logger.debug("Retrieving daemon\"{}\" auto start status",
                service.name);
        String cmd = String.format(GET_AUTO_START_STATUS_CMD_TEMPLATE, service.name);
        CommandResults results = executeCommand(cmd);

        if (results.getExitCode() == 0) {
            logger.debug("For daemon \"{}\" exit code command = {}." +
                            " Daemon is currently configured to start on the next reboot.",
                    service.name, results.getExitCode());
            return true;
        } else {
            logger.debug("For daemon \"{}\" exit code command = {}." +
                            " Daemon is not is currently configured to start on the next reboot.",
                    service.name, results.getExitCode());
            return false;
        }
    }
}
