package org.dat92tashk.osservicesmanagement.environment.unix.systemd;

import org.dat92tashk.osservicesmanagement.Host;
import org.dat92tashk.osservicesmanagement.environment.Descriptive;
import org.dat92tashk.osservicesmanagement.executecmd.CommandExecute;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResults;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResultsCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;

public class SystemDDescription extends SystemDShell implements Descriptive {
    private static final Logger logger
            = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    public SystemDDescription(CommandExecute executor, CommandResultsCache cache) {
        super(executor, cache);
    }

    public String performGetServiceDescription(Host.Service service) {
        logger.debug("Performing getting daemon \"{}\" description", service.name);
        CommandResults results = getServiceParameters(service.name);

        if (results.getExitCode() == 4) {
            logger.debug("Daemon \"{}\" is dead (not found)", service.name);
            return "";
        }

        String daemonName = null;

        String separator = "●";
        for (String curLine : results.getLines()) {
            if (curLine.contains(separator)) {
                daemonName = curLine.substring(curLine.indexOf(separator)+1).trim();
            }
        }
        if (daemonName == null) {
            logger.warn("Daemon \"{}\" description was not parsed", service.name);
            return "";
        }

        logger.trace("Parsing string \"{}\"", daemonName);

        if (daemonName.contains(" - ")) {
            String description = daemonName.substring(daemonName.indexOf(" - ") + 3).trim();
            logger.debug("Received description \"{}\" of the \"{}\" daemon",
                    description, service.name);
            return description;
        } else {
            logger.debug("Daemon has not description");
            return "";
        }

    }
}
