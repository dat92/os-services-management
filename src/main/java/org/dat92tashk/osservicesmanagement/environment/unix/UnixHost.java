package org.dat92tashk.osservicesmanagement.environment.unix;

import org.dat92tashk.osservicesmanagement.Host;
import org.dat92tashk.osservicesmanagement.Manageable;
import org.dat92tashk.osservicesmanagement.OSType;
import org.dat92tashk.osservicesmanagement.environment.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;

public class UnixHost extends Host {
    private static final Logger logger
            = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    private Killable killable;
    private Disable disable;
    private Reloaded reloaded;
    private Autostart autostart;
    private Descriptive descriptive;

    public UnixHost(String hostName,
                    Manageable manageable,
                    Autostart autostart,
                    Killable killable,
                    Disable disable,
                    Reloaded reloaded,
                    Descriptive descriptive) {
        super(hostName, manageable);
        this.autostart = autostart;
        logger.debug("UnixHost \"{}\" created with \"{}\" autoStart value",
                hostName, autostart.getClass().getSimpleName());

        this.killable = killable;
        logger.debug("UnixHost \"{}\" created with \"{}\" killable value",
                hostName, killable.getClass().getSimpleName());

        this.disable = disable;
        logger.debug("UnixHost \"{}\" created with \"{}\" disable value",
                hostName, disable.getClass().getSimpleName());

        this.reloaded = reloaded;
        logger.debug("UnixHost \"{}\" created with \"{}\" reloaded value",
                hostName, reloaded.getClass().getSimpleName());

        this.descriptive = descriptive;
        logger.debug("UnixHost \"{}\" created with \"{}\" description value",
                hostName, descriptive.getClass().getSimpleName());

        logger.info("UnixHost \"{}\" was successfully created", hostName);
        rereadAllExistServices();
    }

    @Override
    public OSType getHostType() {
        return OSType.UNIX;
    }

    @Override
    public void updateServiceParameters(String name) {
        Daemon daemon;
        logger.debug("Processing \"{}\" daemon of \"{}\" UnixHost", name, hostName);
        if (serviceExist(name)) {
            logger.debug("Daemon \"{}\" existed and will be updated", name);
            daemon = (Daemon) services.get(name);
        }
        else {
            logger.debug("Daemon \"{}\" not existed and will be created", name);
            daemon = new Daemon(name);
            services.put(name, daemon);
        }
        Host.ServiceStatus status = manageable.performGetServiceStatus(daemon);
        daemon.changeStatus(status);
        daemon.disabled = disable.performGetDisabledStatus(daemon);
        daemon.description = descriptive.performGetServiceDescription(daemon);
        daemon.autoRun = autostart.getAutoStartStatus(daemon);
        daemon.pid = (status == ServiceStatus.Active) ?
                Integer.parseInt(killable.performGetPIDService(daemon)) : -1;
        logger.info("Daemon parameters \"{}\" of \"{}\" UnixHost was updated", daemon, hostName);
    }


    @Override
    public void rereadAllExistServices() {
        logger.debug("Running reread of \"{}\" UnixHost daemons", hostName);
        List<String> newDaemons = manageable.performGetNamesAllExistServices();
        if (newDaemons == null) {
            logger.error("Failed to collect daemons names on the \"{}\" UnixHost", hostName);
            return;
        } else if (newDaemons.size() == 0) {
            logger.warn("The number of daemons on the host \"{}\" is zero", hostName);
        }
        logger.info("Names of daemons \"{}\" UnixHost were retried", hostName);
        logger.debug("Running updating daemons parameters on the \"{}\" UnixHost", hostName);
        newDaemons.forEach(this::updateServiceParameters);
        logger.info("Daemons UnixHost \"{}\" were reread", hostName);
    }

    public Daemon getDaemon(String name) {
        return (Daemon) getService(name);
    }

    public Autostart getAutostart() {
        return autostart;
    }

    public void setAutostart(Autostart autostart) {
        this.autostart = autostart;
        logger.info("\"{}\" UnixHosts autoStart value changed to \"{}\"",
                hostName, autostart.getClass().getSimpleName());
    }

    public Killable getKillable() {
        return killable;
    }

    public void setKillable(Killable killable) {
        this.killable = killable;
        logger.info("\"{}\" UnixHosts killable value changed to \"{}\"",
                hostName, killable.getClass().getSimpleName());
    }

    public Disable getDisable() {
        return disable;
    }

    public void setDisable(Disable disable) {
        this.disable = disable;
        logger.info("\"{}\" UnixHosts disable value changed to \"{}\"",
                hostName, disable.getClass().getSimpleName());
    }

    public Reloaded getReloaded() {
        return reloaded;
    }

    public void setReloaded(Reloaded reloaded) {
        this.reloaded = reloaded;
        logger.info("\"{}\" UnixHosts reloaded value changed to \"{}\"",
                hostName, reloaded.getClass().getSimpleName());
    }

    public Descriptive getDescriptive() {
        return descriptive;
    }

    public void setDescriptive(Descriptive descriptive) {
        this.descriptive = descriptive;
        logger.info("\"{}\" UnixHosts descriptive value changed to \"{}\"",
                hostName, descriptive.getClass().getSimpleName());
    }

    public String[] getAllServicesDescriptions() {
        ArrayList<String> descriptions = services.values().stream()
                .map(service -> (Daemon) service)
                .map(daemon -> daemon.description)
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
        return descriptions.toArray(new String[0]);
    }

    public boolean killService(Daemon daemon) {
        return killable.performKillService(daemon);
    }

    public void enableService(Daemon daemon) {
        disable.performEnableService(daemon);
    }

    public void disableService(Daemon daemon) {
        disable.performDisableService(daemon);
    }

    public void reloadService(Daemon daemon) {
        reloaded.performReloadService(daemon);
    }

    public void enableServiceAutoStart(Daemon daemon) {
        autostart.performMakeAutoRun(daemon);
    }

    public void disableServiceAutoStart(Daemon daemon) {
        autostart.performUnmakeAutoRun(daemon);
    }

    public class Daemon extends Service {
        private int pid;
        private boolean disabled;
        private boolean autoRun;
        private String description;

        protected Daemon(String name) {
            super(name);
        }

        private void changeStatus(ServiceStatus status) {
            logger.debug("Changing the status of the \"{}\" service." +
                    " Previous={}, New={}", name, this.status, status);
            previousStatus = this.status;
            this.status = status;
        }

        public String getDescription() {
            return description;
        }

        public int getPid() {
            return pid;
        }

        public boolean isDisabled() {
            return disabled;
        }

        public boolean isAutoRun() {
            return autoRun;
        }

        public boolean kill() {
            return killService(this);
        }

        public void disable() {
            disableService(this);
        }

        public void enable() {
            enableService(this);
        }

        public void reload() {
            reloadService(this);
        }

        public void enableAutoStart() {
            enableServiceAutoStart(this);
        }

        public void disableAutoStart() {
            disableServiceAutoStart(this);
        }

        @Override
        public String toString() {
            return "Daemon{" +
                    "name='" + name + '\'' +
                    ", description='" + description + '\'' +
                    ", status=" + status +
                    ", previousStatus=" + previousStatus +
                    ", disabled=" + disabled +
                    ", autoRun=" + autoRun +
                    ", pid=" + pid +
                    '}';
        }
    }
}
