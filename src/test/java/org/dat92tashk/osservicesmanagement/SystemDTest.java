package org.dat92tashk.osservicesmanagement;

import org.dat92tashk.osservicesmanagement.environment.ServicesManagementSoftware;
import org.dat92tashk.osservicesmanagement.environment.unix.UnixHost;
import org.dat92tashk.osservicesmanagement.environment.unix.systemd.*;
import org.dat92tashk.osservicesmanagement.executecmd.CommandExecute;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResults;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResultsCache;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResultsCacheFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;

public class SystemDTest {
    private static CommandResultsCache cache;
    private static UnixHost testHost;
    private static List<String> daemonsNames;

    @BeforeAll
    private static void init() {
        cache = CommandResultsCacheFactory.getCache("localhost");
        testHost = (UnixHost) HostFactory.getHost("localhost",
                OSType.UNIX,
                ServicesManagementSoftware.Empty);
        daemonsNames = new LinkedList<>();
        daemonsNames.add("accounts-daemon.service");
        daemonsNames.add("alsa-restore.service");
        daemonsNames.add("anacron.service");
        daemonsNames.add("auditd.service");
        daemonsNames.add("live-tools.service");
        daemonsNames.add("networking.service");
        daemonsNames.add("nfs-config.service");
        daemonsNames.add("systemd-fsck@dev-disk-by\\x2duuid-5599beb6\\x2d6f75\\x2d44a8\\x2d846d\\x2d16a7e0f9f8a7.service");
        daemonsNames.add("systemd-fsck@dev-disk-by\\x2duuid-969a2dda\\x2dbc7b\\x2d476b\\x2d979f\\x2d6daa1add0ba7.service");
        daemonsNames.add("systemd-fsck@dev-mapper-nix_system\\x2dhome.service");
        daemonsNames.add("systemd-fsck@dev-mapper-nix_system\\x2dopt.service");
        daemonsNames.add("vboxautostart-service.service");
        
        testHost.setManageable(new Manageable() {
            @Override
            public boolean performStartService(Host.Service service) {
                return false;
            }

            @Override
            public boolean performStopService(Host.Service service) {
                return false;
            }

            @Override
            public List<String> performGetNamesAllExistServices() {
                return daemonsNames;
            }

            @Override
            public Host.ServiceStatus performGetServiceStatus(Host.Service service) {
                return Host.ServiceStatus.Unknown;
            }
        });
        testHost.rereadAllExistServices();
    }

    private static String[] getMockData(String fileName) {
        List<String> data;
        String filePath = String.format("target%stest-classes%s%s",
                File.separatorChar, File.separatorChar, fileName);
        try {
            data = Files.readAllLines(
                    Paths.get(filePath),
                    StandardCharsets.UTF_8);
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return data.toArray(new String[0]);
    }

    private CommandExecute getDaemonsNamesMock() {
        CommandResults allDaemonsNames = new CommandResults(0,
                SystemDManage.GET_ALL_DAEMONS_CMD,
                getMockData("systemdGetAllDaemons"));
        CommandExecute mock = Mockito.mock(CommandExecute.class);
        Mockito.when(mock.execute(SystemDManage.GET_ALL_DAEMONS_CMD)).thenReturn(allDaemonsNames);
        return mock;
    }

    private CommandExecute getDaemonsActiveStatusMock() {
        Answer<CommandResults> answer = invocationOnMock -> {

            String cmd = (String) invocationOnMock.getArguments()[0];
            if (String.format(SystemDManage.GET_DAEMON_STAT_CMD_TEMPLATE, daemonsNames.get(0))
                    .equals(cmd)) {
                return new CommandResults(0,
                        String.format(SystemDManage.GET_DAEMON_STAT_CMD_TEMPLATE, daemonsNames.get(0)),
                        getMockData("activeStatus"));
            } else if (String.format(SystemDManage.GET_DAEMON_STAT_CMD_TEMPLATE, daemonsNames.get(2))
                    .equals(cmd)) {
                return new CommandResults(3,
                        String.format(SystemDManage.GET_DAEMON_STAT_CMD_TEMPLATE, daemonsNames.get(2)),
                        getMockData("inactiveStatus"));
            } else if (String.format(SystemDManage.GET_DAEMON_STAT_CMD_TEMPLATE, daemonsNames.get(5))
                    .equals(cmd)) {
                return new CommandResults(3,
                        String.format(SystemDManage.GET_DAEMON_STAT_CMD_TEMPLATE, daemonsNames.get(5)),
                        getMockData("failedStatus"));
            } else {
                return new CommandResults(-1,
                        "", new String[0]);
            }
        };

        CommandExecute mock = Mockito.mock(CommandExecute.class);
        Mockito.when(mock.execute(anyString())).then(answer);
        return mock;
    }

    private CommandExecute getDaemonStatusMock() {
        ArrayList<String> commands = new ArrayList<>();

        for (int i = 0; i < 12; i++) {
            commands.add(String.format(SystemDDescription.GET_DAEMON_PARAM_TEMPLATE,
                    daemonsNames.get(i)));
        }

        Answer<CommandResults> answer = (Answer<CommandResults>) invocationOnMock -> {
            String cmd = (String) invocationOnMock.getArguments()[0];
            final String status = "_status";
            if (cmd.equals(commands.get(0))) {
                return new CommandResults(0,
                        commands.get(0),
                        getMockData(daemonsNames.get(0) + status));
            } else if (cmd.equals(commands.get(1))) {
                return new CommandResults(0,
                        commands.get(1),
                        getMockData(daemonsNames.get(1) + status));
            } else if (cmd.equals(commands.get(2))) {
                return new CommandResults(3,
                        commands.get(2),
                        getMockData(daemonsNames.get(2) + status));
            } else if (cmd.equals(commands.get(3))) {
                return new CommandResults(4,
                        commands.get(3),
                        getMockData(daemonsNames.get(3) + status));
            } else if (cmd.equals(commands.get(4))) {
                return new CommandResults(3,
                        commands.get(4),
                        getMockData(daemonsNames.get(4) + status));
            } else if (cmd.equals(commands.get(5))) {
                return new CommandResults(3,
                        commands.get(5),
                        getMockData(daemonsNames.get(5) + status));
            } else if (cmd.equals(commands.get(6))) {
                return new CommandResults(3,
                        commands.get(6),
                        getMockData(daemonsNames.get(6) + status));
            } else if (cmd.equals(commands.get(7))) {
                return new CommandResults(0,
                        commands.get(7),
                        getMockData(daemonsNames.get(7) + status));
            } else if (cmd.equals(commands.get(8))) {
                return new CommandResults(0,
                        commands.get(8),
                        getMockData(daemonsNames.get(8) + status));
            } else if (cmd.equals(commands.get(9))) {
                return new CommandResults(0,
                        commands.get(9),
                        getMockData(daemonsNames.get(9) + status));
            } else if (cmd.equals(commands.get(10))) {
                return new CommandResults(0,
                        commands.get(10),
                        getMockData(daemonsNames.get(10) + status));
            } else if (cmd.equals(commands.get(11))) {
                return new CommandResults(0,
                        commands.get(11),
                        getMockData(daemonsNames.get(11) + status));
            } else {
                return new CommandResults(-1, "", new String[0]);
            }
        };
        CommandExecute mock = Mockito.mock(CommandExecute.class);
        Mockito.when(mock.execute(anyString())).then(answer);
        return mock;
    }

    private CommandExecute getAutoStartMock() {
        Answer<CommandResults> answer = (Answer<CommandResults>) invocationOnMock -> {
            String cmd = (String) invocationOnMock.getArguments()[0];
            ArrayList<String> commands = new ArrayList<>();
            for (String curDaemon : daemonsNames) {
                commands.add(String.format(SystemDAutoStart.GET_AUTO_START_STATUS_CMD_TEMPLATE, curDaemon));
            }

            if (cmd.equals(commands.get(0))) {
                return new CommandResults(0,
                        commands.get(0),
                        getMockData("autoStartEnabled"));
            } else if (cmd.equals(commands.get(1))) {
                return new CommandResults(0,
                        commands.get(1),
                        getMockData("autoStartStatic"));
            } else if (cmd.equals(commands.get(3))) {
                return new CommandResults(1,
                        commands.get(3),
                        getMockData("autoStartFailed"));
            } else if (cmd.equals(commands.get(4))) {
                return new CommandResults(1,
                        commands.get(4),
                        getMockData("autoStartMasked"));
            } else {
                return new CommandResults(-1, "", new String[0]);
            }
        };
        CommandExecute mock = Mockito.mock(CommandExecute.class);
        Mockito.when(mock.execute(anyString())).then(answer);
        return mock;
    }

    @Test
    void testPIDDaemons() {
        SystemDKill kill = new SystemDKill(getDaemonStatusMock(), cache);
        assertEquals("638",
                kill.performGetPIDService(testHost.getDaemon(daemonsNames.get(0))));
        assertEquals("640",
                kill.performGetPIDService(testHost.getDaemon(daemonsNames.get(1))));
        assertEquals("3277",
                kill.performGetPIDService(testHost.getDaemon(daemonsNames.get(2))));
        assertEquals("-1",
                kill.performGetPIDService(testHost.getDaemon(daemonsNames.get(3))));
        assertEquals("-1",
                kill.performGetPIDService(testHost.getDaemon(daemonsNames.get(4))));
        assertEquals("738",
                kill.performGetPIDService(testHost.getDaemon(daemonsNames.get(5))));
        assertEquals("600",
                kill.performGetPIDService(testHost.getDaemon(daemonsNames.get(6))));
        assertEquals("528",
                kill.performGetPIDService(testHost.getDaemon(daemonsNames.get(7))));
        assertEquals("543",
                kill.performGetPIDService(testHost.getDaemon(daemonsNames.get(8))));
        assertEquals("544",
                kill.performGetPIDService(testHost.getDaemon(daemonsNames.get(9))));
        assertEquals("535",
                kill.performGetPIDService(testHost.getDaemon(daemonsNames.get(10))));
        assertEquals("-1",
                kill.performGetPIDService(testHost.getDaemon(daemonsNames.get(11))));
    }

    @Test
    void testDisableStatus() {
        SystemDDisable disable = new SystemDDisable(getDaemonStatusMock(), cache);
        assertFalse(disable.performGetDisabledStatus(testHost.getDaemon(daemonsNames.get(0))));
        assertFalse(disable.performGetDisabledStatus(testHost.getDaemon(daemonsNames.get(2))));
        assertFalse(disable.performGetDisabledStatus(testHost.getDaemon(daemonsNames.get(3))));
        assertTrue(disable.performGetDisabledStatus(testHost.getDaemon(daemonsNames.get(4))));
    }

    @Test
    void testAutoStart() {
        SystemDAutoStart autoStart = new SystemDAutoStart(getAutoStartMock(), cache);
        assertTrue(autoStart.getAutoStartStatus(testHost.getDaemon(daemonsNames.get(0))));
        assertTrue(autoStart.getAutoStartStatus(testHost.getDaemon(daemonsNames.get(1))));
        assertFalse(autoStart.getAutoStartStatus(testHost.getDaemon(daemonsNames.get(3))));
        assertFalse(autoStart.getAutoStartStatus(testHost.getDaemon(daemonsNames.get(4))));

    }

    @Test
    void testDescriptions() {
        SystemDDescription description = new SystemDDescription(getDaemonStatusMock(), cache);
        assertEquals("Accounts Service",
                description.performGetServiceDescription(testHost.getDaemon(daemonsNames.get(0))));
        assertEquals("Save/Restore Sound Card State",
                description.performGetServiceDescription(testHost.getDaemon(daemonsNames.get(1))));
        assertEquals("Run anacron jobs",
                description.performGetServiceDescription(testHost.getDaemon(daemonsNames.get(2))));
        assertEquals("",
                description.performGetServiceDescription(testHost.getDaemon(daemonsNames.get(3))));
        assertEquals("",
                description.performGetServiceDescription(testHost.getDaemon(daemonsNames.get(4))));
        assertEquals("Raise network interfaces",
                description.performGetServiceDescription(testHost.getDaemon(daemonsNames.get(5))));
        assertEquals("Preprocess NFS configuration",
                description.performGetServiceDescription(testHost.getDaemon(daemonsNames.get(6))));
        assertEquals("File System Check on /dev/disk/by-uuid/5599beb6-6f75-44a8-846d-16a7e0f9f8a7",
                description.performGetServiceDescription(testHost.getDaemon(daemonsNames.get(7))));
        assertEquals("File System Check on /dev/disk/by-uuid/969a2dda-bc7b-476b-979f-6daa1add0ba7",
                description.performGetServiceDescription(testHost.getDaemon(daemonsNames.get(8))));
        assertEquals("File System Check on /dev/mapper/nix_system-home",
                description.performGetServiceDescription(testHost.getDaemon(daemonsNames.get(9))));
        assertEquals("File System Check on /dev/mapper/nix_system-opt",
                description.performGetServiceDescription(testHost.getDaemon(daemonsNames.get(10))));
        assertEquals("",
                description.performGetServiceDescription(testHost.getDaemon(daemonsNames.get(11))));
    }

    @Test
    void testGetNames() {
        SystemDManage manage = new SystemDManage(getDaemonsNamesMock(), cache);
        List<String> names = manage.performGetNamesAllExistServices();
        assertEquals(names.size(), 12);
        for (int i = 0; i < names.size(); i++) {
            assertEquals(names.get(i), daemonsNames.get(i));
        }
    }

    @Test
    void testDaemonsStatus() {
        SystemDManage manage = new SystemDManage(getDaemonsActiveStatusMock(), cache);
        assertSame(manage.performGetServiceStatus(testHost.getService(daemonsNames.get(0))),
                Host.ServiceStatus.Active);
        assertSame(manage.performGetServiceStatus(testHost.getService(daemonsNames.get(2))),
                Host.ServiceStatus.Inactive);
        assertSame(manage.performGetServiceStatus(testHost.getService(daemonsNames.get(5))),
                Host.ServiceStatus.Inactive);

    }

}
