package org.dat92tashk.osservicesmanagement.environment.unix.systemd;

import org.dat92tashk.osservicesmanagement.Host;
import org.dat92tashk.osservicesmanagement.executecmd.CommandExecute;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResults;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResultsCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;

public class SystemDDisable extends SystemDShell implements org.dat92tashk.osservicesmanagement.environment.Disable {
    private static final Logger logger
            = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    public SystemDDisable(CommandExecute executor, CommandResultsCache cache) {
        super(executor, cache);
    }

    public void performDisableService(Host.Service service) {

    }

    @Override
    public void performEnableService(Host.Service service) {

    }

    @Override
    public boolean performGetDisabledStatus(Host.Service service) {
        logger.debug("Retrieving daemon \"{}\" disable status\"",
                service.name);
        CommandResults results = getServiceParameters(service.name);

        if (results.getExitCode() == 3) {
            for (String curLine : results.getLines()) {
                if (curLine.contains("Loaded:")) {
                    if (curLine.contains("masked")) {
                        logger.debug("Daemon \"{}\" is disable (masked)",
                                service.name);
                        return true;
                    }
                }
            }
        }
        logger.debug("Daemon \"{}\" is enable (loaded)",
                service.name);
        return false;
    }
}
