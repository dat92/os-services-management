package org.dat92tashk.osservicesmanagement.environment.unix.systemd;

import org.dat92tashk.osservicesmanagement.Host;
import org.dat92tashk.osservicesmanagement.environment.Reloaded;
import org.dat92tashk.osservicesmanagement.executecmd.CommandExecute;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResultsCache;

public class SystemDReload extends SystemDShell implements Reloaded {

    public SystemDReload(CommandExecute executor, CommandResultsCache cache) {
        super(executor, cache);
    }

    @Override
    public void performReloadService(Host.Service service) {

    }
}
