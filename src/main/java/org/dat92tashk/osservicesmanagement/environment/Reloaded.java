package org.dat92tashk.osservicesmanagement.environment;

import org.dat92tashk.osservicesmanagement.Host;

public interface Reloaded extends ServiceOperation {
    void performReloadService(Host.Service service);
}
