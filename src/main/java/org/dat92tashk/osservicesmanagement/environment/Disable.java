package org.dat92tashk.osservicesmanagement.environment;

import org.dat92tashk.osservicesmanagement.Host;

public interface Disable extends ServiceOperation {
    void performDisableService(Host.Service service);
    void performEnableService(Host.Service service);
    boolean performGetDisabledStatus(Host.Service service);
}
