package org.dat92tashk.osservicesmanagement.environment;

import org.dat92tashk.osservicesmanagement.Host;

public interface Killable extends ServiceOperation {
    boolean performKillService(Host.Service service);
    String performGetPIDService(Host.Service service); //In some OS PID may be not integer
}
