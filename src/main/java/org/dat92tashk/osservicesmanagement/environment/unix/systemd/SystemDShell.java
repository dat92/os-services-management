package org.dat92tashk.osservicesmanagement.environment.unix.systemd;

import org.dat92tashk.osservicesmanagement.executecmd.CommandExecute;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResults;
import org.dat92tashk.osservicesmanagement.executecmd.CommandResultsCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;

public class SystemDShell {
    private static final Logger logger
            = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

    public static final String GET_DAEMON_PARAM_TEMPLATE = "systemctl status" + " \"%s\" " + "-n 0";

    private final CommandResultsCache cache;
    private CommandExecute executor;

    public SystemDShell(CommandExecute executor, CommandResultsCache cache) {
        this.cache = cache;
        this.executor = executor;
    }

    protected CommandResults executeCommand(String cmd) {
        CommandResults results = cache.getResults(cmd);
        if (results != null) {
            logger.trace("Result of \"{}\" commands was obtained from cache ", cmd);
            return results;
        }
        else {
            logger.trace("Result of \"{}\" commands missing in cache ", cmd);
            results = executor.execute(cmd);
            cache.put(cmd, results);
            return results;
        }
    }

    protected CommandResults getServiceParameters(String name) {
        logger.debug("Getting \"{}\" daemon parameters", name);
        String cmd = String.format(GET_DAEMON_PARAM_TEMPLATE, name);
        CommandResults result;
        result = cache.getResults(name);
        if (result == null) {
            logger.trace("Daemon \"{}\" parameters missing in cache", name);
            result = executor.execute(cmd);
            cache.put(name, result);
        } else {
            logger.trace("Daemon \"{}\" parameters was obtained from cache", name);
        }
        return result;
    }
}
