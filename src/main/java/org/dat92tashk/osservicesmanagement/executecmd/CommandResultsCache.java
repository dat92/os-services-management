package org.dat92tashk.osservicesmanagement.executecmd;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import java.time.Duration;

public class CommandResultsCache {
    private volatile Cache<String, CommandResults> cache = CacheBuilder.newBuilder()
            .expireAfterWrite(Duration.ofSeconds(10))
            .build(new CacheLoader<String, CommandResults>() {
                @Override
                public CommandResults load(String s) throws Exception {
                    return null;
                }
            });

    CommandResultsCache() {}

    public synchronized CommandResults getResults(String serviceName) {
        return cache.getIfPresent(serviceName);
    }

    public synchronized void put(String service, CommandResults results) {
        cache.put(service, results);
    }
}
