package org.dat92tashk.osservicesmanagement;

import java.util.List;

public interface Manageable {
    boolean performStartService(Host.Service service);
    boolean performStopService(Host.Service service);
    List<String> performGetNamesAllExistServices();
    Host.ServiceStatus performGetServiceStatus(Host.Service service);
}
